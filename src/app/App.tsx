import './App.css';
import { Layout } from 'antd';
import { PostsListPage } from '../post/list/PostsListPage';
import { AppHeader } from './AppHeader';
import { BrowserRouter as Router, Routes, Route, Navigate } from 'react-router-dom';
import { PostDetailsPage } from '../post/details/PostDetailsPage';

const { Header, Footer, Content } = Layout;

export function App() {
  return (
    <Router>
      <Layout className="App">
        <Header className="AppHeader">
          <AppHeader />
        </Header>
        <Content className="AppContent">
          <Routes>
            <Route path="/" element={<Navigate to="/posts" replace />} />
            <Route path="/posts" element={<PostsListPage />} />
            <Route path="/posts/:postId" element={<PostDetailsPage />} />
          </Routes>
        </Content>
        <Footer>Test task</Footer>
      </Layout>
    </Router>
  );
}
