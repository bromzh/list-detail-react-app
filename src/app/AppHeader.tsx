import { HomeOutlined } from '@ant-design/icons';
import { FC } from 'react';
import { Link } from 'react-router-dom';

export const AppHeader: FC = () => {
  return (
    <Link to="/">
      <HomeOutlined />
    </Link>
  );
};
