import { runInAction } from 'mobx';
import { observer } from 'mobx-react';
import { FC, useEffect } from 'react';
import { postsStore } from '../PostStore';
import { PostsList } from './PostsList';

export const PostsListPage: FC = observer(() => {
  useEffect(() => {
    postsStore.getPosts().then();
    runInAction(() => {
      postsStore.currentPost = undefined;
    });
  }, []);
  return <PostsList posts={postsStore.posts} />;
});
