import { Typography } from 'antd';
import { FC, memo } from 'react';
import { Post } from '../Post';

interface Props {
  post: Post;
}

export const PostListItem: FC<Props> = memo(({ post }) => {
  return (
    <>
      <Typography.Title level={2}>
        {/* <Link to={`/posts/${post.id}`}> */}
        <Typography.Link href={`/posts/${post.id}`}>{post.title}</Typography.Link>
        {/* </Link> */}
      </Typography.Title>
      <Typography.Paragraph>
        <Typography.Text type="secondary">{post.body}</Typography.Text>
      </Typography.Paragraph>
    </>
  );
});
