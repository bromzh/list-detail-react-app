import { Divider } from 'antd';
import { FC, Fragment, memo } from 'react';
import { Post } from '../Post';
import { PostListItem } from './PostListItem';

interface Props {
  posts: Post[];
}

export const PostsList: FC<Props> = memo(({ posts }) => {
  return (
    <>
      {posts.map(post => (
        <Fragment key={post.id}>
          <PostListItem post={post} />
          <Divider />
        </Fragment>
      ))}
    </>
  );
});
