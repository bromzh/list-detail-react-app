import { ComponentMeta, ComponentStory } from '@storybook/react';
import { PostListItem } from './PostListItem';

export default {
  title: 'Post/Post List Item',
  args: {
    post: {
      userId: 1,
      id: 1,
      title: 'sunt aut facere repellat provident occaecati excepturi optio reprehenderit',
      body: 'quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto',
    },
  },
} as ComponentMeta<typeof PostListItem>;

const Template: ComponentStory<typeof PostListItem> = args => <PostListItem {...args} />;
export const Primary = Template.bind({});
