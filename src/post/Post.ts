import { PostComment } from './comments/Comment';

export interface Post {
  id: number;
  title: string;
  body: string;
  userId: number;
}

export interface PostWithDetails extends Post {
  comments?: PostComment[];
}
