import { Post, PostWithDetails } from './Post';

export class PostsService {
  private baseUrl = 'https://jsonplaceholder.typicode.com';

  async getPosts(): Promise<Post[]> {
    const response = await fetch(`${this.baseUrl}/posts`);
    const posts: Post[] = await response.json();
    return posts;
  }

  async getPostDetails(postId: number): Promise<PostWithDetails> {
    const postResponse = await fetch(`${this.baseUrl}/posts/${postId}`);
    const post: PostWithDetails = await postResponse.json();
    const commentsResponse = await fetch(`${this.baseUrl}/comments?postId=${postId}`);
    const comments = await commentsResponse.json();
    post.comments = comments;
    post.body = [post.body, post.body, post.body, post.body, post.body].join('\n'); // emulate full text for post
    return post;
  }
}

export const postsService = new PostsService();
