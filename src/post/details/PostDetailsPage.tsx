import { Typography } from 'antd';
import { observer } from 'mobx-react';
import { FC, useEffect } from 'react';
import { useParams } from 'react-router';
import { postsStore } from '../PostStore';
import { PostDetail } from './PostDetail';

const empty = <Typography.Text type="secondary">No post</Typography.Text>;

export const PostDetailsPage: FC = observer(() => {
  const params = useParams();
  const postId = Number(params.postId);
  useEffect(() => {
    if (postId != null) {
      postsStore.getPostDetails(postId);
    }
  }, [postId]);
  if (postsStore.postDetailsError) {
    return <Typography.Text type="danger">{postsStore.postDetailsError}</Typography.Text>;
  }
  if (!postsStore.currentPost) {
    return empty;
  }
  return <PostDetail post={postsStore.currentPost} />;
});
