import { ComponentMeta, ComponentStory } from '@storybook/react';
import { PostDetail } from './PostDetail';

export default {
  title: 'Post/Detail',
  component: PostDetail,
  args: {
    post: {
      userId: 1,
      id: 1,
      title: 'sunt aut facere repellat provident occaecati excepturi optio reprehenderit',
      body: 'quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto',
      comments: [
        {
          postId: 1,
          id: 2,
          name: 'quo vero reiciendis velit similique earum',
          email: 'Jayne_Kuhic@sydney.com',
          body: 'est natus enim nihil est dolore omnis voluptatem numquam\net omnis occaecati quod ullam at\nvoluptatem error expedita pariatur\nnihil sint nostrum voluptatem reiciendis et',
        },
      ],
    },
  },
} as ComponentMeta<typeof PostDetail>;

const Template: ComponentStory<typeof PostDetail> = args => <PostDetail {...args} />;

export const PostWithComments = Template.bind({});

export const PostWithoutComments = Template.bind({});
PostWithoutComments.args = {
  post: {
    userId: 1,
    id: 1,
    title: 'sunt aut facere repellat provident occaecati excepturi optio reprehenderit',
    body: 'quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto',
  },
};
