import { FC, memo } from 'react';
import { PostWithDetails } from '../Post';
import { Divider, Typography } from 'antd';
import { CommentsList } from '../comments/CommentsList';

interface Props {
  post: PostWithDetails;
}

export const PostDetail: FC<Props> = memo(({ post }) => {
  return (
    <div className="PostDetail">
      <Typography.Title level={1}>{post.title}</Typography.Title>
      <Typography.Paragraph>{post.body}</Typography.Paragraph>
      <Divider />
      <CommentsList comments={post.comments ?? []} />
    </div>
  );
});
