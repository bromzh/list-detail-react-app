import { action, computed, makeObservable, observable } from 'mobx';
import { Post, PostWithDetails } from './Post';
import { postsService } from './PostService';

export class PostsStore {
  @observable.ref
  posts: Post[] = [];

  @observable
  isPostsLoading = false;

  @observable.ref
  currentPost?: PostWithDetails;

  @observable
  isCurrentPostLoading = false;

  @observable
  postsError?: string;

  @observable
  postDetailsError?: string;

  constructor() {
    makeObservable(this);
  }

  @action
  async getPosts(): Promise<void> {
    this.isPostsLoading = true;
    try {
      this.posts = await postsService.getPosts();
    } catch (e) {
      this.postsError = (e as Error).message;
      this.posts = [];
    } finally {
      this.isPostsLoading = false;
    }
  }

  @action
  async getPostDetails(postId: number): Promise<void> {
    this.isPostsLoading = true;
    try {
      this.currentPost = await postsService.getPostDetails(postId);
    } catch (e) {
      this.postDetailsError = (e as Error).message;
      this.currentPost = undefined;
    } finally {
      this.isPostsLoading = false;
    }
  }
}

export const postsStore = new PostsStore();
