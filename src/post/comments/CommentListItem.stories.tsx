import { ComponentMeta, ComponentStory } from '@storybook/react';
import { CommentListItem } from './CommentListItem';

export default {
  title: 'Post/Comment',
  args: {
    comment: {
      postId: 1,
      id: 2,
      name: 'quo vero reiciendis velit similique earum',
      email: 'Jayne_Kuhic@sydney.com',
      body: 'est natus enim nihil est dolore omnis voluptatem numquam\net omnis occaecati quod ullam at\nvoluptatem error expedita pariatur\nnihil sint nostrum voluptatem reiciendis et',
    },
  },
} as ComponentMeta<typeof CommentListItem>;

const Template: ComponentStory<typeof CommentListItem> = args => <CommentListItem {...args} />;

export const Primary = Template.bind({});
