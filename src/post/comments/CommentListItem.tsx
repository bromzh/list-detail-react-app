import { Divider, Typography } from 'antd';
import { FC, memo } from 'react';
import { PostComment } from './Comment';

interface Props {
  comment: PostComment;
}

export const CommentListItem: FC<Props> = memo(({ comment }) => {
  return (
    <>
      <Typography.Paragraph>{comment.body}</Typography.Paragraph>
      <Divider dashed />
    </>
  );
});
