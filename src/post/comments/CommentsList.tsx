import { Typography } from 'antd';
import { FC, memo } from 'react';
import { PostComment } from './Comment';
import { CommentListItem } from './CommentListItem';

interface Props {
  comments: PostComment[];
}

const Empty = () => {
  return (
    <Typography.Paragraph>
      <Typography.Text type="secondary">No comments yet :(</Typography.Text>
    </Typography.Paragraph>
  );
};

export const CommentsList: FC<Props> = memo(({ comments }) => {
  return (
    <>
      <Typography.Title level={3}>Comments ({comments.length ?? 0})</Typography.Title>
      {comments.length === 0 ? (
        <Empty />
      ) : (
        comments.map(comment => <CommentListItem key={comment.id} comment={comment} />)
      )}
    </>
  );
});
